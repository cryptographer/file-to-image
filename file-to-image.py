from PIL import Image
import argparse
import math


def file_to_image_rgb(input_file_path, output_image_path):
    with open(input_file_path, 'rb') as file:
        file_data = file.read()

    file_data = len(file_data).to_bytes(4, byteorder='big', signed=False) + file_data

    dim = math.ceil(math.sqrt(len(file_data) / 3))
    pixel_count = dim * dim

    pixel_data = []
    for i in range(0, pixel_count * 3, 3):
        pixel = file_data[i:i+3]
        if len(pixel) < 3:
            pixel += b'\x00' * (3 - len(pixel))
        pixel_data.append(tuple(pixel))

    image = Image.new('RGB', (dim, dim))
    image.putdata(pixel_data)
    image.save(output_image_path, 'PNG')


def file_to_image_palette(input_file_path, output_image_path):
    with open(input_file_path, 'rb') as file:
        file_data = file.read()

    file_data = len(file_data).to_bytes(4, byteorder='big', signed=False) + file_data

    dim = math.ceil(math.sqrt(len(file_data)))

    image = Image.new('P', (dim, dim))

    palette = list(range(256))
    image.putpalette(palette)

    image.putdata(file_data)
    image.save(output_image_path, 'PNG')


def image_to_file(input_image_path, output_file_path):
    binary_data = Image.open(input_image_path).tobytes()

    file_length = int.from_bytes(binary_data[0:4], byteorder='big', signed=False)
    binary_data = binary_data[4:]

    with open(output_file_path, 'wb') as file:
        file.write(binary_data[:file_length])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Reversibly encode arbitrary data into a png image file.')
    parser.add_argument('mode', choices=['encode', 'decode'])
    parser.add_argument('encoding', choices=['rgb', 'p'])
    parser.add_argument('input')
    parser.add_argument('output')

    args = parser.parse_args()

    if args.mode == 'encode':
        if args.encoding == 'rgb':
            file_to_image_rgb(args.input, args.output)
        elif args.encoding == 'p':
            file_to_image_palette(args.input, args.output)
    elif args.mode == 'decode':
        image_to_file(args.input, args.output)
