# file-to-image

This is a python utility that reversibly encodes arbitrary data into a png image file.

Included in this repo is an encoded sample audio track for testing, created by [Cloudkicker](https://cloudkicker.bandcamp.com/track/let-yourself-be-huge) and available under [CC-BY](https://creativecommons.org/licenses/by/3.0/).

![Music](cloudkicker.png)
